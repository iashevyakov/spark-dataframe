import os
import typing

import requests
from pyspark.conf import SparkConf
from pyspark.shell import sqlContext
from pyspark.sql import SparkSession, DataFrame
import pygeohash as pgh
from pyspark.sql.types import FloatType, StringType, ArrayType, DoubleType
from pyspark.sql.functions import udf, when, avg

# external API url for getting coordinates by placename
OPENCAGE_URL = os.environ.get("OPENCAGE_URL")

# cloud paths for dataframes
HOTELS_PATH = os.environ.get("HOTELS_PATH")
WEATHER_PATH = os.environ.get("WEATHER_PATH")


# defining spark config providing appName, master, and credentials for cloud
def setup_spark_config() -> SparkConf:
    conf = SparkConf()
    conf.set("fs.azure.account.auth.type.bd201stacc.dfs.core.windows.net", os.environ.get("AUTH_TYPE"))
    conf.set("fs.azure.account.oauth.provider.type.bd201stacc.dfs.core.windows.net", os.environ.get("PROVIDER_TYPE"))
    conf.set("fs.azure.account.oauth2.client.id.bd201stacc.dfs.core.windows.net", os.environ.get("CLIENT_ID"))
    conf.set("fs.azure.account.oauth2.client.secret.bd201stacc.dfs.core.windows.net", os.environ.get("CLIENT_SECRET"))
    conf.set("fs.azure.account.oauth2.client.endpoint.bd201stacc.dfs.core.windows.net", os.environ.get("CLIENT_ENDPOINT"))
    return conf


# user-defined-func for obtaining coords through external API
@udf(returnType=ArrayType(FloatType()))
def coords_udf(*args) -> typing.Tuple[float, float]:
    url = OPENCAGE_URL % ', '.join(args)
    coords_result = requests.get(url).json()["results"][0]["geometry"]
    return coords_result["lat"], coords_result["lng"]


# user-defined-func for obtaining geohash based on coordinates
@udf(returnType=StringType())
def geohash_udf(lat: float, long: float, precision: int = 4) -> str:
    return pgh.encode(lat, long, precision=precision)


# func for casting coords' string-typed-columns to double-type
def cast_coord_cols_to_double(df: DataFrame) -> DataFrame:
    return df.withColumn('Latitude', df.Latitude.cast(DoubleType())).withColumn('Longitude',
                                                                                df.Longitude.cast(DoubleType()))


# func for adding column 'Geohash' to df based on user-defined-func 'get_geohash'
def add_column_geohash(df: DataFrame, lat_col, lng_col) -> DataFrame:
    return df.withColumn('Geohash', geohash_udf(lat_col, lng_col))


# func for replacing null values in coords
def fill_coord_when_null(df: DataFrame) -> DataFrame:
    df = df.withColumn('Latitude', when(df.Latitude.isNull(),
                                        coords_udf(df.Address, df.City, df.Country)[0]).otherwise(df.Latitude))
    df = df.withColumn('Longitude', when(df.Longitude.isNull(),
                                         coords_udf(df.Address, df.City, df.Country)[1]).otherwise(df.Longitude))
    return df


# func for grouping df by (date, geohash) and taking avg-value for temperatures
def group_by_geohash_date(df: DataFrame):
    return df.groupBy("wthr_date", "year", "month", "day", "Geohash").agg(avg("avg_tmpr_f").alias("avg_tmpr_f"),
                                                                          avg("avg_tmpr_c").alias("avg_tmpr_c"))


if __name__ == "__main__":
    # defining session's config
    conf = setup_spark_config()

    # creating spark session based on pre-defined config
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    # read dataframe with hotels
    df_hotels = spark.read.csv(HOTELS_PATH, header=True, inferSchema=True)

    # cast Longitude and Latitude from String to Double
    df_hotels = cast_coord_cols_to_double(df_hotels)

    # replace null values in Latitude, Longitude columns with correct ones (through external API)
    df_hotels = fill_coord_when_null(df_hotels)

    # adding extra column 'Geohash' (with 4-chars values), which based on Latitude, Longitude columns' values
    df_hotels = add_column_geohash(df_hotels, df_hotels.Latitude, df_hotels.Longitude)

    # reading dataframe with weather
    df_weather = spark.read.parquet(WEATHER_PATH)

    # adding extra column 'Geohash' (with 4-chars values), which based on lat, lng columns' values
    df_weather = add_column_geohash(df_weather, df_weather.lat, df_weather.lng)

    # grouping by (date, geohash) and taking avg-value for 'avg_tmpr_f', 'avg_tmpr_c'
    df_weather = group_by_geohash_date(df_weather)

    # left join weather with hotels
    left_join_result = df_weather.join(df_hotels, "Geohash", how="left")

    # taking short-version for saving to local storage (newcomer-azure-account is forbidden now for Russia)
    left_join_result_shorted = sqlContext.createDataFrame(left_join_result.head(20), left_join_result.schema)

    # saving results to local storage
    left_join_result_shorted.write.mode("overwrite").format("parquet").save(os.environ.get("OUTPUT_PATH"))

    spark.stop()
