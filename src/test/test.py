import unittest
from unittest import mock

from pyspark.sql import SparkSession
from pyspark.sql.functions import collect_list
from pyspark.sql.types import FloatType, StructField, StructType, DoubleType, IntegerType, StringType

from src.main.python.main import add_column_geohash, fill_coord_when_null, group_by_geohash_date, \
    cast_coord_cols_to_double


class SparkETLTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.spark = (SparkSession
                     .builder
                     .master("local[*]")
                     .appName("pyspark-app-tests")
                     .getOrCreate())

        # preparing schema for dataframes which needed in test-cases
        cls.input_schema_coord = StructType([
            StructField('Longitude', FloatType()),
            StructField('Latitude', FloatType()),
        ])
        cls.input_schema_weather = StructType([
            StructField('avg_tmpr_f', DoubleType()),
            StructField('avg_tmpr_c', DoubleType()),
            StructField('wthr_date', StringType()),
            StructField('year', IntegerType()),
            StructField('month', IntegerType()),
            StructField('day', IntegerType()),
            StructField('Geohash', StringType())
        ])

    def testAddColumnGeohash(self):

        """ Check dataframe after adding extra column 'Geohash' with 4 chars """

        input_data = [(80.0, 80.0),
                      (75.0, 75.0)]
        input_df = self.spark.createDataFrame(data=input_data, schema=self.input_schema_coord)
        df = add_column_geohash(input_df, input_df.Latitude, input_df.Longitude)

        self.assertTrue("Geohash" in df.columns)

        expected_hashes = ["vy0z", "vtm6"]
        actual_hashes = df.select(collect_list('Geohash')).first()[0]

        self.assertEqual(expected_hashes, actual_hashes)

    @mock.patch('src.main.python.main.coords_udf')
    def testFillCoordWhenNull(self, coords_mock):

        """ Check dataframe after replacing null-values in Latitude,Longitude
        with correct coordinates """

        input_schema = StructType([
            StructField('Longitude', FloatType()),
            StructField('Latitude', FloatType()),
            StructField('Address', StringType()),
            StructField('City', StringType()),
            StructField('Country', StringType()),
        ])

        input_data = [(80.0, 80.0, None, None, None),
                      (75.0, 75.0, None, None, None),
                      (None, None, None, None, None)]
        input_df = self.spark.createDataFrame(data=input_data, schema=input_schema)

        # mocking func that obtains coords from external API
        coords_mock.return_value = (70.0, 70.0)

        df = fill_coord_when_null(input_df)

        self.assertTrue(coords_mock.called)

        columns_null_count_dict = {col: df.filter(df[col].isNull()).count() for col in df.columns}

        self.assertEqual(columns_null_count_dict['Latitude'], 0)
        self.assertEqual(columns_null_count_dict['Longitude'], 0)

    def testGroupByDateGeohash(self):

        """ Check dataframe after grouping by (date, geohash)
        and taking avg-value for 'avg_tmpr_f', 'avg_tmpr_c' columns """

        input_data = [
            (20.0, 25.0, "2017-08-29", 2017, 8, 29, "9eem"),
            (30.0, 27.0, "2017-08-29", 2017, 8, 29, "9eem"),
            (30.0, 30.0, "2017-08-30", 2017, 8, 30, "9eem"),
            (5.0, 10.0, "2017-08-30", 2017, 8, 30, "9eec"),
            (9.0, 12.0, "2017-08-30", 2017, 8, 30, "9eec"),
        ]
        input_df = self.spark.createDataFrame(data=input_data, schema=self.input_schema_weather)

        df = group_by_geohash_date(input_df)

        self.assertEqual(df.count(), 3)
        self.assertEqual(len(df.columns), 7)
        self.assertEqual(df.select(collect_list('avg_tmpr_f')).first()[0], [25.0, 30.0, 7.0])
        self.assertEqual(df.select(collect_list('avg_tmpr_c')).first()[0], [26.0, 30.0, 11.0])

    def testCastCoordColsToDouble(self):

        """ Check dataframe after casting columns Latitude, Longitude of hotels-df from String to Double"""

        # define new schema (not from SetUpClass), because String Type for coords is needed
        input_schema_coord = StructType([
            StructField('Longitude', StringType()),
            StructField('Latitude', StringType()),
        ])
        input_data = [("80.0", "80.0"),
                      ("75.0", "75.0")]
        input_df = self.spark.createDataFrame(data=input_data, schema=input_schema_coord)

        df = cast_coord_cols_to_double(input_df)

        self.assertEqual(dict(df.dtypes), {"Longitude": "double", "Latitude": "double"})

    @classmethod
    def tearDownClass(cls):
        cls.spark.stop()