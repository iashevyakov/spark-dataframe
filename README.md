**Instruction for use**:


Install virtual env in `venv/` directory

Create .env and fill there you own env-variables' values from `.env.example`

Activate your env-variables by `source venv/bin/activate`

Install requirements via `pip install -r requirements.txt`


For local-run job with `docker` build image:

`docker build -t sparkbasics:1.0 -f ./docker/Docker`

Then use next commands:

`docker run --rm sparkbasics:1.0 driver ./opt/app/main.py`

or

`sudo chmod +x ./scripts/local_run_docker.sh`

`./scripts/local_run_docker.sh`




For local-run job `without docker` use:

`spark-submit --master "local[*]" --name weather --packages org.apache.hadoop:hadoop-azure:3.2.0,com.microsoft.azure:azure-storage:8.6.3 src/main/python/main.py`

or

`sudo chmod +x ./scripts/local_run.sh`

`./scripts/local_run.sh
`

**DF-hotels with string-typed coord-columns:**


![](screenshots/coords_string_type.png)


**DF-hotels with double-typed coord-columns:**


![](screenshots/coords_cast_double.png)


**DF-hotels with null values:**


![](screenshots/hotels_with_null.png)


**DF-hotels after replacing nulls and calculated column "Geohash":**


![](screenshots/hotels_without_null_and_with_geohash.png)


**DF-weather with calculated "Geohash" columnn:**


![](screenshots/weather_geohash_column.png)


**DF-weather with dropped duplicates by aggregating:**


![](screenshots/weather_dropped_duplicates.png)


**Left-join result:**

![](screenshots/left_join_result.png)


**Saved result of left-join in local storage:**

![](screenshots/output_file_in_storage.png)


**Docker-run job example (via scripts provided in **scripts** directory):**



![](screenshots/docker_run_example.png)


Local run job without docker:

![](screenshots/local_run_without_docker.png)







